# CrowdMP
CrowdMP is a shared unity set of projects to create and run experiment with all  
sort of crowds.  
It is composed of:  
- A Master project that contains the minimum and necessary code to run any kind of experiement with crowd.  
- A list of AddOns projects that are located in the AddOns subgroup. 

## GIT STRUCTURE

![Gitlab structure of the project](Doc/Media/GitStruct.png)
  
## GIT SUBMODULES DOCUMENTATION  
Submodules can be usefull to manage the different AddOns on separated git for your own fork:
https://www.vogella.com/tutorials/GitSubmodules/article.html  
  
## HOW TO USE CROWDMP AND CREATE AN EXPERIMENT 
See document "[Doc/Doc_CrowdMP.pdf](Doc/Doc_CrowdMP.pdf)" 
