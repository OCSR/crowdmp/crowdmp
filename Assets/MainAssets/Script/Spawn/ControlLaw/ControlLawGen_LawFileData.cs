﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;

//public class LawFileData : ControlLaw
namespace CrowdMP.Core
{
    public class ControlLawGen_LawFileData : ControlLawGen
    {
        [Header("Main Parameters")]
        [XmlAttribute]
        public int timeColumn;
        [XmlAttribute]
        public int xColumn;
        [XmlAttribute]
        public int yColumn;
        [XmlAttribute]
        public int zColumn;

        public string dataFile;



        public override CustomXmlSerializer<ControlLaw> createControlLaw()
        {
            LawFileData law = new LawFileData();
            law.timeColumn = timeColumn;
            law.xColumn = xColumn;
            law.yColumn = yColumn;
            law.zColumn = zColumn;
            law.dataFile = dataFile;

            return law;
        }

        public override ControlLawGen randDraw(GameObject agent, int id = 0)
        {
            ControlLawGen_LawFileData law = agent.AddComponent<ControlLawGen_LawFileData>();

            law.timeColumn = timeColumn;
            law.xColumn = xColumn;
            law.yColumn = yColumn;
            law.zColumn = zColumn;
            law.dataFile = dataFile.Replace("{USER}", id.ToString());

            return law;
        }
    }
}