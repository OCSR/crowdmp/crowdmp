﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    /// <summary>
    /// Configurable recorder using DataRecorder to load a list of data to record from the trial parameters
    /// </summary>
    public class ConfigurableRecorder : MonoBehaviour, Recorder
    {

        private Player player;
        private List<Agent> agents;
        private ToolsOutput recordingObject;
        //private GameObject playerCam;
        private List<RecorderData> dataList;

        /// <summary>
        /// Reset recorder
        /// </summary>
        public void clear()
        {
            if (recordingObject != null)
                recordingObject = null;
            if (agents != null)
            {
                agents.Clear();
                agents = null;
            }
            dataList.Clear();

        }

        /// <summary>
        /// Initialize recorder with an agent list to look after
        /// </summary>
        /// <param name="agentList"> The agents from which to save data </param>
        public void initRecorder(Player p, List<Agent> agentList)
        {
            player = p;
            //playerCam = GameObject.FindGameObjectWithTag("MainCamera");


            // Create data files
            string filePrototype = LoaderConfig.sceneOutputFile;
            if (filePrototype != "")
            {

                string path = LoaderConfig.dataPath + @"/" + LoaderConfig.RecFolder + @"\" + filePrototype;
                path = path.Replace("{USER}", LoaderConfig.xpCurrentUser.ToString());
                path = path.Replace("{ITT}", LoaderConfig.xpCurrentTrial.ToString());

                recordingObject = new ToolsOutput(path);
            }
            else
            {
                recordingObject = null;
                return;
            }

            // Save agents list
            agents = new List<Agent>(agentList);

            // Save Data list
            if (dataList == null)
                dataList = new List<RecorderData>();
            foreach (CustomXmlSerializer<RecorderData> d in LoaderConfig.recordedDataList)
            {
                d.Data.initialize();
                dataList.Add(d.Data);
            }

            if (LoaderConfig.RecHeaders)
            {
                // Print headers
                string dataLine = "Time";

                // Player data
                foreach (RecorderData d in dataList)
                    dataLine = dataLine + d.getPlayerHeader(player);

                // Player data
                foreach (Agent a in agents)
                    foreach (RecorderData d in dataList)
                        dataLine = dataLine + d.getAgentHeader(a);

                // Other data
                foreach (RecorderData d in dataList)
                    dataLine = dataLine + d.getOtherData();

                recordingObject.writeLine(dataLine);
            }
        }

        /// <summary>
        /// Add new agent to record position
        /// </summary>
        /// <param name="agent">the new agent to add</param>
        public void addAgent(Agent agent)
        {
            agents.Add(agent);
        }

        /// <summary>
        /// Removed killed agent to record position
        /// </summary>
        /// <param name="agent">the new agent to add</param>
        public void removeAgent(Agent agent)
        {
            agents.Remove(agent);
        }

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Data are recorded during Late Update when all agent's movements are over (movements are done during update)
        /// </summary>
        void LateUpdate()
        {
            if (ToolsTime.DeltaTime != 0 && recordingObject != null)
            {
                string dataLine = ToolsTime.TrialTime.ToString();

                // Player data
                foreach (RecorderData d in dataList)
                    dataLine = dataLine + d.getPlayerData(player);

                // Player data
                foreach (Agent a in agents)
                    foreach (RecorderData d in dataList)
                        dataLine = dataLine + d.getAgentData(a);

                // Other data
                foreach (RecorderData d in dataList)
                    dataLine = dataLine + d.getOtherData();

                recordingObject.writeLine(dataLine);
            }
        }
    }
}