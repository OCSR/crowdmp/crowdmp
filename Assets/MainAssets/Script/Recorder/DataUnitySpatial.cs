﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Convert agent spatial data to text for recording
    /// </summary>
    public class DataUnitySpatial : RecorderData
    {
        public string getAgentData(Agent a)
        {
            string dataText =
                            LoaderConfig.RecDataSeparator +
                            a.gameObject.transform.position.x.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                            LoaderConfig.RecDataSeparator +
                            a.gameObject.transform.position.y.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                            LoaderConfig.RecDataSeparator +
                            a.gameObject.transform.position.z.ToString().Replace(".", LoaderConfig.RecDecimalSeparator);
            return dataText;
        }

        public string getAgentHeader(Agent a)
        {
            string dataText = LoaderConfig.RecDataSeparator +
                                "Agent " + a.GetID() + " Position X" +
                                LoaderConfig.RecDataSeparator +
                                "Agent " + a.GetID() + " Position Y" +
                                LoaderConfig.RecDataSeparator +
                                "Agent " + a.GetID() + " Position Z";
            return dataText;
        }

        public string getOtherData()
        {
            return "";
        }

        public string getOtherHeader()
        {
            return "";
        }

        public string getPlayerData(Player p)
        {
            GameObject playerCam = GameObject.FindGameObjectWithTag("MainCamera");
            string dataText =   /* Positon PlayerObject */
                               LoaderConfig.RecDataSeparator +
                               p.gameObject.transform.position.x.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                               LoaderConfig.RecDataSeparator +
                               p.gameObject.transform.position.y.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                               LoaderConfig.RecDataSeparator +
                               p.gameObject.transform.position.z.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                               LoaderConfig.RecDataSeparator +

                               /* Position Camera */
                               playerCam.transform.position.x.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                               LoaderConfig.RecDataSeparator +
                               playerCam.transform.position.y.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                               LoaderConfig.RecDataSeparator +
                               playerCam.transform.position.z.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                               LoaderConfig.RecDataSeparator +

                               /* Rotation Camera */
                               playerCam.transform.eulerAngles.x.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                               LoaderConfig.RecDataSeparator +
                               playerCam.transform.eulerAngles.y.ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                               LoaderConfig.RecDataSeparator +
                               playerCam.transform.eulerAngles.z.ToString().Replace(".", LoaderConfig.RecDecimalSeparator);

            return dataText;
        }

        public string getPlayerHeader(Player p)
        {
            GameObject playerCam = GameObject.FindGameObjectWithTag("MainCamera");
            string dataText =   /* Positon PlayerObject */
                               LoaderConfig.RecDataSeparator +
                               "Player Object Position X" +
                               LoaderConfig.RecDataSeparator +
                               "Player Object Position Y" +
                               LoaderConfig.RecDataSeparator +
                               "Player Object Position Z" +

                               /* Position Camera */
                               LoaderConfig.RecDataSeparator +
                               "Player Camera Position X" +
                               LoaderConfig.RecDataSeparator +
                               "Player Camera Position Y" +
                               LoaderConfig.RecDataSeparator +
                               "Player Camera Position Z" +

                               /* Rotation Camera */
                               LoaderConfig.RecDataSeparator +
                               "Player Camera Rotation X" +
                               LoaderConfig.RecDataSeparator +
                               "Player Camera Rotation Y" +
                               LoaderConfig.RecDataSeparator +
                               "Player Camera Rotation Z";

            return dataText;
        }

        public void initialize()
        {

        }
    }
}