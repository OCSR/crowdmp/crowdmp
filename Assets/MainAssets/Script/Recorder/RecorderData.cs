﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Abstract class to define classes converting data to text for the ConfigurableRecorder
    /// </summary>
    public interface RecorderData
    {

        /// <summary>
        /// initialize the data recorder
        /// </summary>
        void initialize();
        /// <summary>
        /// Return data to recorded about the player (String start with delimiter, end without)
        /// </summary>
        /// <param name="p">String containing the player data in CSV format</param>
        /// <returns></returns>
        string getPlayerData(Player p);
        /// <summary>
        /// Return data to recorded about a give agent (String start with delimiter, end without)
        /// </summary>
        /// <param name="a">agent which data are return</param>
        /// <returns>String containing the given agent data in CSV format</returns>
        string getAgentData(Agent a);
        /// <summary>
        /// Return data to recorded about other thing than player or agent (String start with delimiter, end without)
        /// </summary>
        /// <returns>String containing the data in CSV format</returns>
        string getOtherData();
        /// <summary>
        /// Return header for the recorded data about the player (String start with delimiter, end without)
        /// </summary>
        /// <returns>String containing the recorded data list for the player in CSV format</returns>
        string getPlayerHeader(Player p);
        /// <summary>
        /// Return header for the recorded data about the agents (String start with delimiter, end without)
        /// </summary>
        /// <returns>String containing the recorded data list for the agents in CSV format</returns>
        string getAgentHeader(Agent a);
        /// <summary>
        /// Return header for the recorded data that is neither about player nor agents (String start with delimiter, end without)
        /// </summary>
        /// <returns>String containing the recorded data list, for everything neither player nor agents related, in CSV format</returns>
        string getOtherHeader();
    }
}