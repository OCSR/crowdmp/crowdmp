﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    public class LawStanding : ControlLaw
    {

        [XmlAttribute]
        public int animationType; // 0 - No animation | 1 - idle | 2 - talking | 3 - applause | 4 - look around

        public ConfigVect LookAt = new ConfigVect();

        private Agent linkedAgent;
        private Vector3 oldPos;


        public bool computeGlobalMvt(float deltaTime, out Vector3 translation, out Vector3 rotation)
        {
            translation = new Vector3(0, 0, 0);
            rotation = LookAt.vect;

            //float speed = (linkedAgent.transform.position - oldPos).magnitude / deltaTime;

            //if (speed < 0.0001)
            //{
            //    float angle = Vector3.SignedAngle(linkedAgent.transform.forward, LookAt.vect - linkedAgent.transform.position, linkedAgent.transform.up);
            //    rotation = new Vector3(0, Mathf.Min(angle, 180 * deltaTime), 0);
            //}
            //oldPos = linkedAgent.transform.position;
            return true;
        }

        public void initialize(Agent a)
        {
            linkedAgent = a;
            //oldPos = a.transform.position;

            AnimationController ac = a.gameObject.GetComponentInChildren<AnimationController>();
            ac.setAnimationType(animationType);
        }

        public bool applyMvt(Agent a, Vector3 translation, Vector3 rotation)
        {
            a.transform.LookAt(rotation);
            return true;
        }
    }
}