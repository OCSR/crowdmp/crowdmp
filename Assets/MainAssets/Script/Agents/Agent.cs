﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{
    /// <summary>
    /// Agent abstract class to create interactings agents in trials
    /// </summary>
    public abstract class Agent : MonoBehaviour
    {
        internal uint id;
        internal float endTime;
        internal bool isKillable;


        /// <summary>
        /// Get agent id
        /// </summary>
        /// <returns>the id</returns>
        public uint GetID()
        {
            return id;
        }

        /// <summary>
        /// Perform a step of the agent
        /// </summary>
        abstract public void doStep();

        /// <summary>
        /// Give the agent position 
        /// </summary>
        public virtual Vector3 Position { get { return gameObject.transform.position; } }

        /// <summary>
        /// Translate agent position
        /// </summary>
        public virtual void Translate(Vector3 translation) { gameObject.transform.Translate(translation); }

        /// <summary>
        /// Rotate agent position
        /// </summary>
        public virtual void Rotate(Vector3 rotation) { gameObject.transform.Rotate(rotation); }

        /// <summary>
        /// Overide agent goal by simulation results
        /// </summary>
        /// <param name="position">New position</param>
        /// <param name="speed">New speed</param>
        public void simOverride(Vector3 position, Vector3 speed)
        {
            transform.position = position;

            if (speed.sqrMagnitude > 0.001)
            {
                Vector3 LookPos = position + speed;
                transform.LookAt(LookPos);
            }

        }

        /// <summary>
        /// Check if the agent is at the end of its life
        /// </summary>
        /// <returns>Return true iof the agent should be removed</returns>
        abstract public bool toKill();
    }


    /// <summary>
    /// Trial parameters concerning the player's input device (XML serializable)
    /// </summary>
    public abstract class TrialAgent
    {
        [XmlAttribute]
        public string mesh;
        [XmlAttribute]
        public float radius = 0.33f;
        [XmlAttribute]
        public bool killable = false;
        [XmlAttribute]
        public float startLife = 0.0f;
        [XmlAttribute]
        public float endLife = float.MaxValue;


        public abstract Agent createAgentComponnent(GameObject agentObject, uint id);
        public abstract Vector3 getStartingPosition();
        public abstract TrialControlSim getControlSimInfo();
    }
}