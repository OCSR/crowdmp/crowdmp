﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Class managing all the simulations used during the trial
    /// </summary>
    public class SimManager
    {

        List<ControlSim> simulators;
        List<int> agentToSim;

        /// <summary>
        /// Initialize the class
        /// </summary>
        public SimManager()
        {
            simulators = new List<ControlSim>();
            agentToSim = new List<int>();
        }

        /// <summary>
        /// Reset the manager
        /// </summary>
        public void clear()
        {
            foreach (ControlSim sim in simulators)
                sim.clear();
            simulators.Clear();
            agentToSim.Clear();
        }

        /// <summary>
        /// Initialize all the simulations needed for the Trial using the LoaderConfig
        /// </summary>
        public void initSimulations(Obstacles obst)
        {
            //List<TrialControlSim> tmp=new List<TrialControlSim>();

            // Create simulators
            agentToSim.Add(getInternalId(LoaderConfig.playerInfo.getControlSimInfo()));
            //tmp.Add(LoaderConfig.playerInfo.getControlSimInfo());

            foreach (TrialAgent a in LoaderConfig.agentsInfo)
            {
                getInternalId(a.getControlSimInfo());
                //tmp.Add(a.getControlSimInfo());
            }

            // Fill simulators with agents and Obstacles
            int internalID = 0;
            foreach (ControlSim sim in simulators)
            {
                // Agents
                int agent = 0;
                if (agentToSim[agent] == internalID)
                {
                    sim.addAgent(LoaderConfig.playerInfo.getStartingPosition(), LoaderConfig.playerInfo.getControlSimInfo());
                }
                else
                {
                    sim.addNonResponsiveAgent(LoaderConfig.playerInfo.getStartingPosition(), LoaderConfig.playerInfo.radius);
                }
                //foreach (TrialAgent a in LoaderConfig.agentsInfo)
                //{
                //    ++agent;

                //    if (agentToSim[agent] == internalID)
                //    {
                //        sim.addAgent(a.getStartingPosition(), a.getControlSimInfo());
                //    }
                //    else
                //    {
                //        sim.addNonResponsiveAgent(a.getStartingPosition(), a.radius);
                //    }
                //}

                // Obstacles
                sim.addObstacles(obst);

                // update id
                ++internalID;
            }
        }

        public void removeAgent(int id)
        {
            foreach (ControlSim sim in simulators)
            {
                sim.removeAgent(id);
            }
        }

        public void addNewAgent(Agent a, float radius, TrialControlSim infos)
        {
            // Fill simulators with agents and Obstacles
            int internalID = 0;
            agentToSim.Add(getInternalId(infos));
            foreach (ControlSim sim in simulators)
            {
                if (agentToSim[agentToSim.Count - 1] == internalID)
                {
                    sim.addAgent(a.transform.position, infos);
                }
                else
                {
                    sim.addNonResponsiveAgent(a.transform.position, radius);
                }
                // update id
                ++internalID;
            }
        }

        /// <summary>
        /// Check the config id of a simulation and get the internal one if already created, create a new simulations otherwise
        /// </summary>
        /// <param name="info">Trial parameters of the simulation</param>
        /// <returns>Internal id</returns>
        private int getInternalId(TrialControlSim info)
        {
            if (info == null)
                return -1;

            int id = -1;
            int i = 0;
            foreach (ControlSim sim in simulators)
            {
                if (sim.getConfigId() == info.getConfigId())
                {
                    id = i;
                    break;
                }
            }
            if (id < 0)
            {
                id = simulators.Count;
                simulators.Add(info.createControlSim(id));
            }

            return id;
        }

        /// <summary>
        /// Update all the simulations with XP state, perform a simulation step and ovveride controlled agent state
        /// </summary>
        /// <param name="deltaTime">Time since last step</param>
        /// <param name="posList">List of current position fo the agents</param>
        /// <param name="playerGoalState">The player state he is trying to reach</param>
        /// <param name="agentsGoalState">The agent states they are trying to reach</param>
        public void doStep(float deltaTime, List<Vector3> posList, Agent playerGoalState, List<Agent> agentsGoalState)
        {
            int i = 0;

            // Update simulators state
            foreach (ControlSim sim in simulators)
            {
                sim.updateAgentState(0, posList[i], (playerGoalState.Position - posList[i]) / deltaTime);
                foreach (Agent a in agentsGoalState)
                {
                    ++i;
                    sim.updateAgentState((int)a.id, posList[i], (a.Position - posList[i]) / deltaTime);
                }

                sim.doStep(deltaTime);
            }

            int simId = agentToSim[0];
            if (simId >= 0)
                playerGoalState.simOverride(simulators[simId].getAgentPos2d(0), simulators[simId].getAgentSpeed2d(0));

            foreach (Agent a in agentsGoalState)
            {
                simId = agentToSim[(int)a.id];
                if (simId >= 0)
                    a.simOverride(simulators[simId].getAgentPos2d((int)a.id) + new Vector3(0, a.transform.position.y, 0), simulators[simId].getAgentSpeed2d((int)a.id));
            }
        }
    }
}