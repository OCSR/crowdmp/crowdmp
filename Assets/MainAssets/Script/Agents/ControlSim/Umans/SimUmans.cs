﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CrowdMP.Core
{

    public class SimUmans : ControlSim
    {
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // EXPORT C++ FUNCTIONS FROM LIB
        [DllImport("UMANS-Library")]
        private static extern IntPtr UMANS_CreateSimObject(string configFileName, int numberOfThreads);
        [DllImport("UMANS-Library")]
        private static extern void UMANS_DestroySimObject(IntPtr obj);
        [DllImport("UMANS-Library")]
        private static extern void UMANS_doStep(IntPtr obj, float deltaTime);
        

        [DllImport("UMANS-Library")]
        private static extern void UMANS_addObstacle(IntPtr obj, float s_startx, float s_starty, float s_endx, float s_endy);
        [DllImport("UMANS-Library")]
        private static extern bool UMANS_removeAgent(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        private static extern int UMANS_addAgent(IntPtr obj, float pos_x, float pos_y, float radius, float prefSpeed, float maxSpeed, float maxAcceleration, int policyID);
        [DllImport("UMANS-Library")]
        private static extern int UMANS_addObst(IntPtr obj, float[] Px, float[] Py, int numPoint);

        [DllImport("UMANS-Library")]
        private static extern bool UMANS_setPosition(IntPtr obj, int s_indPedestrian, float s_x, float s_y);
        [DllImport("UMANS-Library")]
        private static extern bool UMANS_setVelocity(IntPtr obj, int s_indPedestrian, float s_x, float s_y);
        [DllImport("UMANS-Library")]
        private static extern bool UMANS_setGoal(IntPtr obj, int s_indPedestrian, float s_x, float s_y, float speed);

        [DllImport("UMANS-Library")]
        private static extern float UMANS_getAgentPositionX(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        private static extern float UMANS_getAgentPositionY(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        private static extern float UMANS_getAgentVelX(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        private static extern float UMANS_getAgentVelY(IntPtr obj, int s_indPedestrian);
        // EXPORT C++ FUNCTIONS FROM LIB
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------


        List<int> UIDtoSIMID;

        int ConfigId;
        IntPtr sim;

        public SimUmans(int id)
        {
            ConfigId = id;
            UIDtoSIMID = new List<int>();
            string file = "Assets\\MainAssets\\Script\\Agents\\ControlSim\\Umans\\Config.xml";
            try
            {
                sim = UMANS_CreateSimObject(file, 1);
            }
            catch (InvalidCastException)
            {
            }
        }

        ~SimUmans()
        {
            if (sim != null)
            {
                try
                {
                    UMANS_DestroySimObject(sim);
                }
                catch (InvalidCastException)
                {
                }
            }
        }

        public void removeAgent(int uid)
        {
            UMANS_removeAgent(sim, UIDtoSIMID[uid]);
        }

        public void addAgent(Vector3 position, TrialControlSim infos)
        {
            UmansConfig Uinfos = (UmansConfig)infos;

            int i = -1;
            if (sim != null)
            {
                try
                {
                    i = UMANS_addAgent(sim, -position.x, position.z, Uinfos.radius, Uinfos.prefVel, Uinfos.maxVel, Uinfos.maxAcc, (int) Uinfos.policy);
                    UIDtoSIMID.Add(i);
                }
                catch (InvalidCastException)
                {
                }
            }
            if (i < 0)
                ToolsDebug.logFatalError("Error during agent creation in UMANS");
        }

        public void addNonResponsiveAgent(Vector3 position, float radius)
        {
            int i = -1;
            if (sim != null)
            {
                try
                {
                    i = UMANS_addAgent(sim, -position.x, position.z, radius, 0, 0, 0, (int)UmansConfig.Policy.ORCA);
                    UIDtoSIMID.Add(i);
                }
                catch (InvalidCastException)
                {
                }
            }
            if (i < 0)
                ToolsDebug.logFatalError("Error during non responsive agent creation in UMANS");
        }

        public void addObstacles(Obstacles obst)
        {
            foreach (ObstCylinder pillar in obst.Pillars)
            {
                try
                {
                    UMANS_addAgent(sim, -pillar.position.x, pillar.position.z, pillar.radius, 0, 0, 0, (int)UmansConfig.Policy.BLANK);
                }
                catch (InvalidCastException)
                {
                    ToolsDebug.logFatalError("Error while adding obstacles to Umans");
                }
            }

            foreach (ObstWall wall in obst.Walls)
            {
                Vector3 center = (wall.A + wall.B + wall.C + wall.D) / 4;
                float[] Px = new float[4];
                float[] Py = new float[4]; ;
                if (ObstWall.isClockwise(center, wall.A, wall.B) > 0)
                {
                    Px[0] = -wall.A.x; Px[1] = -wall.B.x; Px[2] = -wall.C.x; Px[3] = -wall.D.x;
                    Py[0] = wall.A.z; Py[1] = wall.B.z; Py[2] = wall.C.z; Py[3] = wall.D.z;
                }
                else
                {
                    Px[0] = -wall.A.x; Px[1] = -wall.D.x; Px[2] = -wall.C.x; Px[3] = -wall.B.x;
                    Py[0] = wall.A.z; Py[1] = wall.D.z; Py[2] = wall.C.z; Py[3] = wall.B.z;
                }
                int nbObst = UMANS_addObst(sim, Px, Py, 4);
                ToolsDebug.log(string.Concat("obst:",nbObst.ToString()));
            }
        }

        public void clear()
        {
            try
            {
                if (sim != null)
                    UMANS_DestroySimObject(sim);
                sim = UMANS_CreateSimObject("test", 1);
            }
            catch (InvalidCastException)
            {
            }
        }

        public void doStep(float deltaTime)
        {
            try
            {
                if (sim != null)
                    UMANS_doStep(sim, deltaTime);
            }
            catch (InvalidCastException)
            {
            }
        }

        public Vector3 getAgentPos2d(int uid)
        {
            try
            {
                if (sim != null)
                    return new Vector3(-UMANS_getAgentPositionX(sim, UIDtoSIMID[uid]), 0, UMANS_getAgentPositionY(sim, UIDtoSIMID[uid]));
            }
            catch (InvalidCastException)
            {
            }

            return new Vector3();
        }

        public Vector3 getAgentSpeed2d(int uid)
        {
            try
            {
                if (sim != null)
                    return new Vector3(-UMANS_getAgentVelX(sim, UIDtoSIMID[uid]), 0, UMANS_getAgentVelY(sim, UIDtoSIMID[uid]));
            }
            catch (InvalidCastException)
            {
            }

            return new Vector3();
        }

        public int getConfigId()
        {
            return ConfigId;
        }

        public void updateAgentState(int uid, Vector3 position, Vector3 goal)
        {
            try
            {
                if (sim != null)
                {
                    bool test = UMANS_setPosition(sim, UIDtoSIMID[uid], -position.x, position.z);
                    bool test2 = UMANS_setGoal(sim, UIDtoSIMID[uid], -position.x - 10*goal.x, position.z + 10*goal.z, goal.magnitude);
                }
            }
            catch (InvalidCastException)
            {

            }

        }
    }
}
