﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    public class UmansConfig : TrialControlSim
    {

        public enum Policy
        {
            BLANK = 0,
            ORCA = 1,
            RVO = 2,
            DUTRA = 3,
            FOEAvoidance = 4,
            KARAMOUZAS = 5,
            MOUSSAID = 6,
            PARIS = 7,
            PLEdestrians = 8,
            PowerLaw = 9,
            SocialForces = 10,
            VANTOLL = 11
        }

        [XmlAttribute("SimulationID")]
        public int id;
        [XmlAttribute]
        public Policy policy;
        [XmlAttribute]
        public float radius;
        [XmlAttribute]
        public float prefVel;
        [XmlAttribute]
        public float maxVel;
        [XmlAttribute]
        public float maxAcc;

        public int getConfigId()
        {
            return (int)id;
        }

        public ControlSim createControlSim(int id)
        {

            return new SimUmans(id);
        }

        public UmansConfig()
        {
            id = 0;
            policy=Policy.ORCA;
            radius=0.33f;
            prefVel = 1.33f; ;
            maxVel=2f;
            maxAcc=5f;
    }
    }
}
