﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Regular agent following a controlLaw and a simulation
    /// </summary>
    public class RegularAgent : Agent
    {
        internal ControlLaw movementController;
        internal bool endReached = false;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Perform a step of the agent
        /// </summary>
        public override void doStep()
        {

            /* Check new player coordinates */
            Vector3 translation;
            Vector3 rotation;
            endReached = !movementController.computeGlobalMvt(ToolsTime.DeltaTime, out translation, out rotation);

            /*Update player coordinates */
            movementController.applyMvt(this, translation, rotation);
        }

        public override bool toKill()
        {
            if (isKillable && (endReached || endTime < ToolsTime.TrialTime))
                return true;
            return false;
        }
    }


    /// <summary>
    /// Trial parameters concerning the player's input device (XML serializable)
    /// </summary>
    public class TrialRegularAgent : TrialAgent
    {
        [XmlAttribute]
        public int visualVariation;
        [XmlAttribute]
        public float animationOffset;
        [XmlAttribute]
        public float heightOffset;

        public ConfigVect Position;
        public ConfigVect Rotation;

        [XmlElement("controlLaw")]
        public CustomXmlSerializer<ControlLaw> xmlControlLaw;
        [XmlElement("controlSim")]
        public CustomXmlSerializer<TrialControlSim> xmlControlSim;

        [XmlIgnore]
        public ControlLaw controlLaw { get { return xmlControlLaw.Data; } }
        [XmlIgnore]
        public TrialControlSim controlSim { get { return xmlControlSim == null ? null : xmlControlSim.Data; } }

        public TrialRegularAgent()
        {
            mesh = "m002";
            visualVariation = 0;
            animationOffset = 0;
            heightOffset = 0;

            Position = new ConfigVect();
            Rotation = new ConfigVect();

            xmlControlLaw = null;
            xmlControlSim = null;
        }

        override public TrialControlSim getControlSimInfo()
        {
            return controlSim;
        }

        override public Agent createAgentComponnent(GameObject agentObject, uint id)
        {
            RegularAgent a = agentObject.AddComponent<RegularAgent>();
            a.id = id;
            a.isKillable = killable;
            a.endTime = endLife;

            agentObject.transform.position = Position.vect;
            agentObject.transform.rotation = Quaternion.Euler(Rotation.vect);

            agentObject.transform.localScale = new Vector3(1, 1 + heightOffset, 1);
            float yOffset = (1.7f * heightOffset) / 2;
            Vector3 tmp = agentObject.transform.position;
            tmp.y += yOffset;
            agentObject.transform.position = tmp;

            CapsuleCollider tmpCap = agentObject.GetComponent<CapsuleCollider>();
            if (tmpCap != null)
            {
                tmp = tmpCap.center;
                tmp.y -= yOffset;
                tmpCap.center = tmp;
            }

            // Init control law
            a.movementController = controlLaw;
            controlLaw.initialize(a);

            AnimationController ac = agentObject.GetComponent<AnimationController>();
            if (ac != null)
                ac.setAnimOffset(animationOffset);
        #if ROCKETBOX
            TextureVariation tv = agentObject.GetComponentInChildren<TextureVariation>();
            if (tv != null)
                tv.SetVariationId(visualVariation);
        #endif

            return a;
        }

        override public Vector3 getStartingPosition()
        {
            return Position.vect;
        }
    }
}