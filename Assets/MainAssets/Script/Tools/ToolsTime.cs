﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if MIDDLEVR
using Hybrid.VRTools;
#endif

namespace CrowdMP.Core
{
    /// <summary>
    /// Control Experiment and Trials time (can be modified for multi-platform and time synchronization) 
    /// </summary>
    public static class ToolsTime
    {

        private static float deltaTime;
        private static float realDeltaTime;
        private static float absoluteTime;
        private static float trialTime;
        private static float timeSinceTrialStarted;
        private static bool bPause;

        static ToolsTime()
        {
            realDeltaTime = 0;
            deltaTime = 0;
            absoluteTime = 0;
            trialTime = 0;
            timeSinceTrialStarted = 0;
            bPause = true;
        }

        public static bool isInPause { get { return bPause; } }
        public static float AbsoluteTime { get { return absoluteTime; } }
        public static float TrialTime { get { return trialTime; } }
        public static float TimeSinceTrialStarted { get { return timeSinceTrialStarted; } }
        public static float DeltaTime { get { return deltaTime; } }
        public static float RealDelatTime { get { return realDeltaTime; } }



        public static void updateTime()
        {
#if MIDDLEVR
            realDeltaTime = isInPause ? 0 : VRTool.GetDeltaTime();
#else
            realDeltaTime = Time.deltaTime;
#endif
            timeSinceTrialStarted += realDeltaTime;
            absoluteTime += realDeltaTime;

            if (bPause)
                deltaTime = 0;
            else
            {
                trialTime += realDeltaTime;
                deltaTime = realDeltaTime;
            }
        }

        public static void pauseAndResumeGame(bool stop)
        {
            bPause = stop;
        }

        public static bool tooglePause()
        {
            pauseAndResumeGame(!bPause);
            return bPause;
        }


        public static void newLevel()
        {
            deltaTime = 0;
            trialTime = 0;
            timeSinceTrialStarted = 0;
            bPause = true;
        }

    }
}